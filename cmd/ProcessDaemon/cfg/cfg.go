package cfg

import (
	"io/ioutil"

	"gopkg.in/yaml.v3"
)

type Conf struct {
	Config Config
}
type Config struct {
	Passce []Passce
}

type Passce struct {
	RunName string //进程名
	RunDir  string
	RunExe  string
	RunParm string //运行参数
	Remarks string
}

func GetConf() *Conf {
	yamlfile, err := ioutil.ReadFile("conf.yml")
	if err != nil {
		println(err)
		return nil
	}
	cf := &Conf{}
	err = yaml.Unmarshal(yamlfile, cf)
	if err != nil {
		println(err)
		return nil
	}
	return cf
}

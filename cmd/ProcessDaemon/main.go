package main

import (
	"sync"
	"time"

	"gitee.com/phkill/super_tools/cmd/ProcessDaemon/cfg"
	"gitee.com/phkill/super_tools/tools/logger"
	"gitee.com/phkill/super_tools/tools/process"
)

func main() {
	Start()
}

func Start() {
	cf := cfg.GetConf()
	if cf == nil {
		return
	}
	log := logger.NewLog()
	wi := sync.WaitGroup{}

	for _, v := range cf.Config.Passce {
		wi.Add(1)
		ps := process.NewProcess(v.RunExe, v.RunDir, v.RunParm, log)
		ps.SetStartTime(time.Second * 10)
		ps.SetEvent(func(Stats int, name string) {
			switch Stats {
			case process.ProcessStatsStart, process.ProcessStatsStop, process.ProcessStatsWait:
				log.Debug(name, nil)

			case process.ProcessStatsError, process.ProcessStopError, process.ProcessStatsNil:
				log.ErrorMsg(name)
			}
		})
		go func(wi *sync.WaitGroup, ps *process.ProcessWait) {
			defer wi.Done()
			//ps.FindProcessToKillId()
			ps.Start1()
		}(&wi, ps)
	}
	wi.Wait()
}

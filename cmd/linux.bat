SET CGO_ENABLED=0
SET GOARCH=amd64
SET GOOS=linux

go build -ldflags "-s -w" -o processDeamon


go env -w GOOS=windows

go env -w GOOS=linux
go env -w CGO_ENABLED=0
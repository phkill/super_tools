module gitee.com/phkill/super_tools

go 1.18

require (
	github.com/TeaOSLab/EdgeCommon v0.5.5
	github.com/iwind/TeaGo v0.0.0-20220811034530-657e3f15b79e
	github.com/iwind/gosock v0.0.0-20220505115348-f88412125a62
	github.com/kardianos/service v1.2.2
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/miekg/dns v1.1.50
	github.com/suiyunonghen/DxCommonLib v0.5.3
	github.com/suiyunonghen/dxsvalue v0.3.3
	go.uber.org/zap v1.23.0
	golang.org/x/sys v0.1.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/jonboulle/clockwork v0.3.0 // indirect
	github.com/lestrrat-go/strftime v1.0.6 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/automaxprocs v1.5.1 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.6-0.20210726203631-07bc1bf47fb2 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)

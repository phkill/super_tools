package process

import (
	"bufio"
	"fmt"
	"gitee.com/phkill/super_tools/tools/logger"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	ProcessStatsNil   = 0
	ProcessStatsStart = 1
	ProcessStatsStop  = 2
	ProcessStatsWait  = 3
	ProcessStatsError = 4
	ProcessStopError  = 5
)

type ProcessWait struct {
	Dir          string
	Name         string
	Parm         string
	lock         sync.Mutex
	log          *logger.MulLogger
	isStop       bool
	currexe      *exec.Cmd
	ProcessStats int
	event        func(Stats int, name string)
	Args         []string
	statTime     time.Duration
	closechan    chan int
	lastTime     time.Time
	isrun        bool
}

func NewProcess(name, dir, param string, log *logger.MulLogger) *ProcessWait {
	return &ProcessWait{Name: name, Dir: dir, Parm: param, lock: sync.Mutex{}, log: log, ProcessStats: ProcessStatsNil, statTime: time.Second * 5, closechan: make(chan int, 1)}
}

func NewProcessArr(name, dir string, log *logger.MulLogger, arg ...string) *ProcessWait {
	sp := &ProcessWait{Name: name, Dir: dir, log: log, Args: append([]string{name}, arg...), statTime: time.Second * 5, closechan: make(chan int, 1)}
	return sp
}

func (p *ProcessWait) Strop() {
	p.FindProcessToKillId()
}

func (p *ProcessWait) SetEvent(even func(Stats int, name string)) {
	p.event = even
}
func (p *ProcessWait) KeEvent(stats int, name string) {
	p.ProcessStats = stats
	if p.event != nil {
		p.event(stats, name)
	}
}

func (p *ProcessWait) StartNodeWeb() {
	sp := filepath.Join(p.Dir, "/", p.Name)
	_, er := os.Stat(sp)
	if er != nil {
		fmt.Printf("文件不存 启动失败 %s", filepath.Join(p.Dir, p.Name))
		if p.log != nil {
			p.log.ErrorMsg("文件不存 启动失败 %s", filepath.Join(p.Dir, p.Name))
		}
		time.Sleep(time.Second * 10)
		return
	}
	exec.Command("chmod", "777", sp).CombinedOutput()
	p.isStop = false
	p.FindProcessToKillId()
	for {
		if p.isStop {
			return
		}
		cexe := exec.Command(sp, p.Args[1], p.Args[2])
		cexe.Dir = filepath.Dir(sp)
		p.currexe = cexe
		er = cexe.Start()
		p.KeEvent(ProcessStatsStart, p.Name)
		if er == nil {
			cexe.Wait()
		} else if er != nil {
			p.KeEvent(ProcessStatsError, p.Name)
		}
		p.KeEvent(ProcessStatsStop, p.Name)

		p.FindProcessToKillId()
		time.Sleep(time.Second * 15)
	}
}
func (p *ProcessWait) SetStartTime(sTime time.Duration) {
	p.statTime = sTime
}

func (p *ProcessWait) Start() {
	p.isStop = false
	vLog := filepath.Join(p.Dir, p.Name+".log")
	os.Remove(vLog)
	time.Sleep(time.Second)
	fi, er := os.Create(vLog)
	if er != nil {
		p.KeEvent(ProcessStatsError, p.Name+"创建日志文件出错 :"+p.Name+".log")
		return
	}
	bw := bufio.NewWriterSize(fi, 10)

	for {
		select {
		case <-p.closechan:
			{
				p.isStop = true
				return
			}
		default:
			{
				if p.IsRun() {
					//程序还在运行，等待结束
					time.Sleep(time.Second * 10)
					p.log.DebugMsg("%s is run", p.Name)
					continue
				}
				sp := filepath.Join(p.Dir, p.Name)
				_, er := os.Stat(sp)
				if er != nil {
					fmt.Printf("文件不存 启动失败 %s", filepath.Join(p.Dir, p.Name))
					if p.log != nil {
						p.log.ErrorMsg("文件不存 启动失败 %s", filepath.Join(p.Dir, p.Name))
					}
					time.Sleep(time.Second * 10)
					continue
				}
				sfile := filepath.Join(p.Dir, p.Name)
				sdir := filepath.Dir(p.Dir + "/")
				sparm := strings.Split(p.Parm, " ")
				var cexe *exec.Cmd
				switch len(sparm) {
				case 0:
					cexe = exec.Command("/bin/nohup", sfile)
				case 1:
					if sparm[0] == "" {
						cexe = exec.Command("/bin/nohup", sfile, "&")
					} else {
						cexe = exec.Command("/bin/nohup", sfile, sparm[0], "&")
					}

				case 2:
					cexe = exec.Command(sfile, sparm[0], sparm[1])
				case 3:
					cexe = exec.Command(sfile, sparm[0], sparm[1], sparm[2])
				case 4:
					cexe = exec.Command(sfile, sparm[0], sparm[1], sparm[2], sparm[3])
				case 5:
					cexe = exec.Command(sfile, sparm[0], sparm[1], sparm[2], sparm[3], sparm[4])
				}
				//cexe := exec.Command(sfile,)
				cexe.Dir = sdir

				//cexe.Stdout = os.Stdout
				//cexe.Stdin = os.Stdin
				//cexe.Stderr = os.Stderr
				cexe.Stdout = bw
				cexe.Stderr = bw
				cexe.Stdin = os.Stdin

				er = cexe.Run()
				p.KeEvent(ProcessStatsStart, fmt.Sprintf("start %s", p.Name))
				if er == nil {
					p.lastTime = time.Now()
					p.isrun = p.IsRun()
					continue

				} else if er != nil {
					p.KeEvent(ProcessStatsError, fmt.Sprintf("start Error %s", p.Name))
				}
				p.isrun = false
				temtime := p.lastTime.Add(time.Second * 10)
				if temtime.Unix() > time.Now().Unix() {
					//启动程序过少,出现了问题，快解决
					p.KeEvent(ProcessStatsError, fmt.Sprintf("程序启动出错 name:%s, path:%s", p.Name, p.Dir))
				}

				p.KeEvent(ProcessStatsError, fmt.Sprintf("start Error %s", p.Name))
				//p.FindProcessToKillId()
				time.Sleep(p.statTime)
			}

		}

	}
}

func (p *ProcessWait) Start1() {
	p.isStop = false
	//vLog := filepath.Join(p.Dir, p.Name+".log")
	//os.Remove(vLog)
	time.Sleep(time.Second)
	//fi, er := os.Create(vLog)
	//if er != nil {
	//	p.KeEvent(ProcessStatsError, p.Name+"创建日志文件出错 :"+p.Name+".log")
	//	return
	//}
	//bw := bufio.NewWriterSize(fi, 10)
	var kcom *exec.Cmd
	for {
		select {
		case <-p.closechan:
			{
				p.isStop = true
				return
			}
		default:
			{
				if p.IsRun() {
					//程序还在运行，等待结束
					p.log.DebugMsg("%s is run", p.Name)
					time.Sleep(time.Second * 10)

					continue
				}
				if kcom != nil {
					kcom.Process.Kill()
				}
				sp := filepath.Join(p.Dir, p.Name)
				_, er := os.Stat(sp)
				if er != nil {
					fmt.Printf("文件不存 启动失败 %s", filepath.Join(p.Dir, p.Name))
					if p.log != nil {
						p.log.ErrorMsg("文件不存 启动失败 %s", filepath.Join(p.Dir, p.Name))
					}
					time.Sleep(time.Second * 10)
					continue
				}

				er, kcom = p.startProcess()
				p.KeEvent(ProcessStatsStart, fmt.Sprintf("start %s", p.Name))
				if er == nil {
					p.lastTime = time.Now()
					p.isrun = p.IsRun()
					continue

				} else if er != nil {
					p.KeEvent(ProcessStatsError, fmt.Sprintf("start Error %s", p.Name))
				}
				p.isrun = false
				temtime := p.lastTime.Add(time.Second * 10)
				if temtime.Unix() > time.Now().Unix() {
					//启动程序过少,出现了问题，快解决
					p.KeEvent(ProcessStatsError, fmt.Sprintf("程序启动出错 name:%s, path:%s", p.Name, p.Dir))
				}

				p.KeEvent(ProcessStatsError, fmt.Sprintf("start Error %s", p.Name))
				//p.FindProcessToKillId()
				time.Sleep(p.statTime)
			}

		}

	}
}
func (p *ProcessWait) Stop() {
	p.closechan <- 1
	p.FindProcessToKillId()
	for {
		if p.isStop {
			return
		}
		time.Sleep(time.Second * 1)
	}
}

//只运行一次
func (p *ProcessWait) StartOne() {
	p.isStop = false
	cexe := exec.Command(filepath.Join(p.Dir, p.Name), filepath.Dir(p.Dir))
	cexe.Dir = filepath.Dir(p.Dir)
	p.FindProcessToKillId()
	p.currexe = cexe
	cexe.CombinedOutput()
}

func (p *ProcessWait) FindProcessToKillId() bool {
	cmd := exec.Command("ps", "-C", p.Name)
	cmdout, err := cmd.Output()
	if err != nil {
		p.log.ErrorMsg(err.Error())
		return false
	}
	files := strings.Fields(string(cmdout))
	osprr := p.parm(files, p.Name)
	return p.Killall(osprr)
}

func (p *ProcessWait) Killall(ospr map[int]*os.Process) bool {
	for _, pr := range ospr {
		er := pr.Kill()
		if er != nil {
			if p.log != nil {
				p.log.ErrorMsg("kill process %s %v", p.Name, er)
			}
			return false
		}
	}
	return true
}
func (p *ProcessWait) IsStatedName(ProcessName string) bool {
	cmd := exec.Command("ps", "-C", ProcessName)
	cmdout, _ := cmd.Output()
	files := strings.Fields(string(cmdout))
	osprr := p.parm(files, ProcessName)
	osid := os.Getpid()
	isboole := false
	for _, pr := range osprr {
		if pr.Pid != osid {
			isboole = true
		}
	}
	if isboole {
		return true
	}
	return false
}

func (p *ProcessWait) IsStated() (*os.Process, bool) {
	cmd := exec.Command("ps", "-C", p.Name)
	cmdout, _ := cmd.Output()
	files := strings.Fields(string(cmdout))
	osprr := p.parm(files, p.Name)
	osid := os.Getpid()
	isboole := false
	var vpr *os.Process
	for _, pr := range osprr {
		if pr.Pid != osid {
			isboole = true
			vpr = pr
			break
		}
	}
	if isboole {
		return vpr, true
	} else {
		return nil, false
	}
}

func (p *ProcessWait) FindProcessId() int {
	cmd := exec.Command("ps", "-C", p.Name)
	cmdout, _ := cmd.Output()
	files := strings.Fields(string(cmdout))
	osprr := p.parm(files, p.Name)
	for _, pr := range osprr {
		if pr.Pid > 0 {
			return pr.Pid
		}
	}
	return 0
}

func (p *ProcessWait) IsRun() bool {
	p.isrun = p.FindProcessId() > 0
	return p.isrun
}

func (p *ProcessWait) parm(files []string, name string) map[int]*os.Process {
	index := -1
	rs := map[int]*os.Process{}
	for _, str := range files {
		if str == "CMD" {
			index = 0
			continue
		}
		if str == name {
			index = 0
			continue
		}
		if index == 0 {
			index = -1
			sid, er := strconv.Atoi(str)
			if er != nil {
				if p.log != nil {
					p.log.ErrorMsg("process 转换id失败 %v", er)
				}

			}
			id := &os.Process{
				Pid: sid,
			}
			rs[sid] = id
		}

	}
	return rs
}

func (p *ProcessWait) startProcess() (error, *exec.Cmd) {
	runs := "cd " + p.Dir + "\nnohup ./" + p.Name + " " + p.Parm + ">nohup.out 2>&1 &"
	err := ioutil.WriteFile(fmt.Sprintf("./%s.sh", p.Name), []byte(runs), 0777)
	if err != nil {
		return err, nil
	}
	cx := exec.Command("/bin/bash", "-c", runs)
	cx.Start()

	if err != nil {
		println(err.Error())
	}
	return err, cx
}

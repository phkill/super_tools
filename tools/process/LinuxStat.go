package process

import (
	"bytes"
	"fmt"
	"gitee.com/phkill/super_tools/tools/logger"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
)

type LinuxStat struct {
	log  *logger.MulLogger
	lock sync.Mutex

	MesterAIP string
	isModify  bool
	Process   map[string]*ProcessWait
	IsStart   bool
}

//优化Tcp
var OptimicaionLinuxTxt = []string{"net.ipv4.tcp_max_tw_buckets = 6000", "net.ipv4.tcp_sack = 1", "net.ipv4.tcp_window_scaling = 1",
	"net.ipv4.tcp_rmem = 4096        87380   4194304", "net.ipv4.tcp_wmem = 4096        16384   4194304",
	"net.core.wmem_default = 8388608", "net.core.rmem_default = 8388608", "net.core.rmem_max = 16777216",
	"net.core.rmem_max = 16777216", "net.core.wmem_max = 16777216", "net.core.netdev_max_backlog = 262144"}
var OptimicationLinuxTxtFile = "/etc/sysctl.conf"

/*
	wrfi.WriteString(`echo "*               soft    nproc             1024000" >> /etc/security/limits.conf`)
	wrfi.WriteString(`echo "*               hard    nproc             1024000" >> /etc/security/limits.conf`)
	wrfi.WriteString(`echo "*          soft    nproc     1024000" >> /etc/security/limits.d/20-nproc.conf`)
	wrfi.WriteString(`echo "*          hard    nproc     1024000" >> /etc/security/limits.d/20-nproc.conf`)
	wrfi.WriteString(`echo "ulimit -SHn 1024000" >> /etc/profile`)
	wrfi.WriteString(`source /etc/profile`)
*/

//优化opfile 数
var OptimicationLinuxTxt1 = []string{"*               soft    nproc             1024000", "*               hard    nproc             1024000"}
var OptimicationLinuxTxt2 = []string{"*          soft    nproc     1024000", "*          hard    nproc     1024000"}
var OptimicationLiuxTxt1File = "/etc/security/limits.conf"
var OptimicationLiuxTxt2File = "/etc/security/limits.d/20-nproc.conf"

var OptimicationLinuxTxt3 = []string{"ulimit -SHn 1024000"}
var OptimicationLinuxTxt3File = "/etc/profile"

var linuxRuntxt = []string{"cd root", "nohup ./OptiLinuxStart &"}
var LinuxRuntxtFile = "/etc/rc.local"

func NewLinuxStart(MesterIP string, log *logger.MulLogger) *LinuxStat {
	return &LinuxStat{log: log, MesterAIP: MesterIP, lock: sync.Mutex{}, Process: map[string]*ProcessWait{}}
}
func (l *LinuxStat) Optionicaion(isVipAdd bool) {
	l.isModify = false
	if !l.isReboot() {
		fmt.Println("修改了TCP参数")
		if l.WriteFile(OptimicaionLinuxTxt, OptimicationLinuxTxtFile) {
			//l.DisableFirewalld()
			fmt.Println("关闭了防火墙")
			if l.WriteFile(OptimicationLinuxTxt1, OptimicationLiuxTxt1File) {
				//fmt.Println("修改了open file 数")
				//if l.WriteFile(OptimicationLinuxTxt2, OptimicationLiuxTxt2File) {
				//	fmt.Println("修改了open file 数 2")
				if l.WriteFile(OptimicationLinuxTxt3, OptimicationLinuxTxt3File) {
					fmt.Println("修改了 etc/profile")
					//if l.WriteFile(linuxRuntxt, LinuxRuntxtFile) {
					//	fmt.Println("修改了开机启动")
					if l.isModify {
						l.runSource()
						l.saveReboot()
						//l.runReboot()
						return

					}
					//}
				}
				//	}
			}
		}
	} else {
		l.log.Debug(" 已经优化linu 不需要优化", nil)
	}
	l.log.Debug(" linux 优化完成", nil)
	if isVipAdd {
		l.log.DebugMsg("开始添加 linux Vip %s", l.MesterAIP)
		l.ADDVip()
		l.log.Debug("   linux Vip 添加完成", nil)
	}

}

func (l *LinuxStat) StartProcess() {
	if l.IsStart {
		return
	}
	l.IsStart = true
	for _, v := range l.Process {
		go v.Start()
	}
}

func (l *LinuxStat) AddProcess(name, dir, param string) error {
	l.lock.Lock()
	defer l.lock.Unlock()
	ps, ok := l.Process[name]
	if ok {
		if ps.Name == name || ps.Dir == dir {
			return nil
		}
		ps.Strop()

	}
	l.Process[name] = &ProcessWait{Name: name, Dir: dir, Parm: param}
	return nil

}

func (l *LinuxStat) DisableFirewalld() {
	exec.Command("systemctl", "stop", "firewalld.service").Run()
	exec.Command("systemctl", "disable", "firewalld.service").Run()
}

func (l *LinuxStat) FindProcess(name string) *ProcessWait {
	l.lock.Lock()
	defer l.lock.Unlock()
	ps, ok := l.Process[name]
	if ok {
		return ps
	}
	return nil
}

func (l *LinuxStat) DelProcess(name string) error {
	l.lock.Lock()
	defer l.lock.Unlock()
	ps, ok := l.Process[name]
	if ok {
		ps.Strop()
		ps.FindProcessToKillId()
		delete(l.Process, name)
		return nil
	}
	return nil
}

func (l *LinuxStat) ADDVip() {
	exec.Command("ip", "addr", "add", l.MesterAIP+"/32", "dev", "lo").CombinedOutput()
	exec.Command("sysctl", "-w", "net.ipv4.conf.lo.arp_ignore=1").CombinedOutput()
	exec.Command("echo", "1", ">", "/proc/sys/net/ipv4/conf/lo/arp_ignore").CombinedOutput()
	exec.Command("echo", "2", ">", "/proc/sys/net/ipv4/conf/lo/arp_announce").CombinedOutput()
	exec.Command("echo", "1", ">", "/proc/sys/net/ipv4/conf/all/arp_ignore").CombinedOutput()
	exec.Command("echo", "2", ">", "/proc/sys/net/ipv4/conf/all/arp_announce").CombinedOutput()
}

func (l *LinuxStat) runSource() {
	_, er := exec.Command("/bin/sh", "-c", "source /etc/profile").CombinedOutput()
	if er != nil {
		l.log.ErrorMsg("执行Source 错误，%v", er)
		return
	}

	return
}
func (l *LinuxStat) SetBootToRestat() {
	path, _ := os.Getwd()
	name := filepath.Base(os.Args[0])
	rename := "rebootSh.sh"
	bufwr := bytes.NewBuffer([]byte{})
	bufwr.WriteString("cd " + path + "\n")
	bufwr.WriteString(fmt.Sprintf("nohup ./%s > log.log 2>&1 &\n", name))
	bufwr.WriteString("exit")
	ioutil.WriteFile(rename, bufwr.Bytes(), 777)
	os.Chmod("/etc/rc.local", 777)
	webdeplay := []string{filepath.Join(path + "/" + rename), " "}
	os.Chmod(rename, 777)

	if l.WriteFile(webdeplay, LinuxRuntxtFile) {
		l.log.Debug("添加开机启动成功", nil)
	}
}

func (l *LinuxStat) WriteFile(req []string, finame string) bool {
	l.lock.Lock()
	defer l.lock.Unlock()
	_, er := os.Stat(finame)
	if er != nil {
		os.Create(finame)
	}
	sbuf, er := ioutil.ReadFile(finame)

	if er != nil {
		l.log.ErrorMsg("读取文件出错 %s %v", finame, er)
		return false
	}
	strs := strings.Split(string(sbuf), "\n")
	add := []int{}
	vsadd := false
	for i, s := range req {
		isadd := false
		for _, v := range strs {
			if strings.Index(v, s) != -1 {
				isadd = true
				break
			}

		}
		if !isadd {
			vsadd = true
			l.isModify = true
			add = append(add, i)
		}
	}
	vbuf := bytes.NewBuffer(sbuf)
	for _, va := range add {
		vbuf.WriteString("\n" + req[va])
	}
	if !vsadd {
		return true
	}

	er = ioutil.WriteFile(finame, vbuf.Bytes(), 0777)
	if er != nil {
		l.log.ErrorMsg("写入文件出错 %s %v", finame, er)
		return false
	}
	return true

}

func (l *LinuxStat) isReboot() bool {
	finame, _ := os.Getwd()
	finame = filepath.Join(finame, "/readb.dat")
	buf, er := ioutil.ReadFile(finame)
	if er != nil {
		return false
	}
	if strings.TrimSpace(string(buf)) == "okRebood" {
		return true
	}
	return false
}
func (l *LinuxStat) saveReboot() {
	finame, _ := os.Getwd()
	finame = filepath.Join(finame, "/readb.dat")
	sbuf := []byte("okRebood")
	er := ioutil.WriteFile(finame, sbuf, 0666)
	if er != nil {
		l.log.ErrorMsg("写入readb.dat 出错 %v", er)
		return
	}
}

func (l *LinuxStat) runReboot() {
	_, er := exec.Command("reboot").CombinedOutput()
	if er != nil {
		l.log.ErrorMsg("执行重启失败，%v", er)
		return
	}
	os.Exit(1)
	return
}

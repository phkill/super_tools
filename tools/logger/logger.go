package logger

import (
	"context"
	"fmt"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/suiyunonghen/DxCommonLib"
	"github.com/suiyunonghen/dxsvalue"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

type LoggerInterface interface {
	DebugMsg(format string, v ...interface{})
	WarnMsg(format string, v ...interface{})
	ErrorMsg(format string, v ...interface{})
	PanicMsg(format string, v ...interface{})
	DPanicMsg(format string, v ...interface{})
	InfoMsg(format string, v ...interface{})
	Debug(msg string, FieldValue interface{})
	Warn(msg string, FieldValue interface{})
	Info(msg string, FieldValue interface{})
	Error(msg string, FieldValue interface{})
	Panic(msg string, FieldValue interface{})
	DPanic(msg string, FieldValue interface{})
}

type LogGatherStyle uint8

const (
	LGS_TCPServer LogGatherStyle = iota
	LGS_File
	LGS_Console
	LGS_MSGQueue
)

type LogGatherItem struct {
	GatherStyle LogGatherStyle //采集方式
	TargetPos   string         //目标地址
	Level       zapcore.Level
}

func (item *LogGatherItem) canLevelGather(lev zapcore.Level) bool {
	return lev >= item.Level
}

type ConsoleWriter struct{}

func (w ConsoleWriter) Write(p []byte) (n int, err error) {
	fmt.Print(DxCommonLib.FastByte2String(p))
	return len(p), nil
}

func NewLog() *MulLogger {
	return NewMultiLogger(true, true, "", context.Background(), LogGatherItem{
		LGS_Console, "", zapcore.DebugLevel,
	})
}

type LoggerPluginHandler func(level zapcore.Level, moduleName, msg string)

//多路路线日志，支持输出到控制台
//支持客户端连接上来，下发日志
type MulLogger struct {
	logger              *zap.Logger
	moduleName          string
	showCaller          bool
	loggerPluginHandles []LoggerPluginHandler
}

func (l *MulLogger) RegisterPlugin(plugin LoggerPluginHandler) {
	l.loggerPluginHandles = append(l.loggerPluginHandles, plugin)
}

//日志配置
type LogConfig struct {
	EnableConsole bool
	DebugMode     bool
	ShowCaller    bool
	LogPath       string
	LogsrvAddr    string
	LogLevel      zapcore.Level
}

func (l *MulLogger) record2ZapField(r *dxsvalue.DxValue) []zap.Field {
	var result []zap.Field
	idx := 0
	if l.moduleName != "" {
		result = make([]zap.Field, r.Count()+1)
		result[idx] = zap.Any("Module", l.moduleName)
		idx++
	} else {
		result = make([]zap.Field, r.Count())
	}
	r.Visit(func(keyName string, value *dxsvalue.DxValue) bool {
		switch value.DataType {
		case dxsvalue.VT_Binary, dxsvalue.VT_Object, dxsvalue.VT_Array:
			result[idx] = zap.ByteString(keyName, value.Binary())
		case dxsvalue.VT_String:
			result[idx] = zap.String(keyName, value.String())
		case dxsvalue.VT_Int:
			result[idx] = zap.Int(keyName, int(value.Int()))
		case dxsvalue.VT_True, dxsvalue.VT_False:
			result[idx] = zap.Bool(keyName, value.Bool())
		case dxsvalue.VT_Float:
			result[idx] = zap.Float32(keyName, value.Float())
		case dxsvalue.VT_Double:
			result[idx] = zap.Float64(keyName, value.Double())
		default:
			return true
		}
		idx++
		return true
	})
	return result
}

func (l *MulLogger) map2Field(v map[string]interface{}) []zap.Field {
	var result []zap.Field
	idx := 0
	if l.moduleName != "" {
		result := make([]zap.Field, len(v)+1)
		result[idx] = zap.Any("Module", l.moduleName)
		idx++
	} else {
		result = make([]zap.Field, len(v))
	}
	for k, value := range v {
		result[idx] = zap.Any(k, value)
		idx++
	}
	return result
}

var (
	bufferpool = sync.Pool{
		New: func() interface{} {
			return make([]byte, 0, 256)
		},
	}
)

func (l *MulLogger) caller() string {
	//找到他的上上上一级的调用位置,0是当前位置exeplugin，1是上一个位置是loger，所以是上上一级
	_, file, line, ok := runtime.Caller(3)
	if !ok {
		return ""
	}
	buffer := bufferpool.Get().([]byte)
	idx := strings.LastIndexByte(file, '/')
	if idx == -1 {
		buffer = append(buffer[:0], file...)
	} else {
		idx = strings.LastIndexByte(file[:idx], '/')
		if idx == -1 {
			buffer = append(buffer[:0], file...)
		} else {
			buffer = append(buffer[:0], file[idx+1:]...)
		}
	}
	/*buffer = append(buffer,'.')
	funcName := runtime.FuncForPC(pc).Name()
	buffer = append(buffer,funcName...)*/
	buffer = append(buffer, ':')
	buffer = strconv.AppendInt(buffer, int64(line), 10)
	result := string(buffer)
	bufferpool.Put(buffer)
	return result
}

func (l *MulLogger) execPlugin(level zapcore.Level, msg string) {
	if l.showCaller {
		msg = msg + "		Caller: " + l.caller()
	}
	for _, plugin := range l.loggerPluginHandles {
		plugin(level, l.moduleName, msg)
	}
}

func (l *MulLogger) DebugMsg(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	if l.moduleName != "" {
		l.logger.Debug(msg, zap.Any("Module", l.moduleName))
	} else {
		l.logger.Debug(msg)
	}
	l.execPlugin(zapcore.DebugLevel, msg)
}

func (l *MulLogger) WarnMsg(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	if l.moduleName != "" {
		l.logger.Warn(msg, zap.Any("Module", l.moduleName))
	} else {
		l.logger.Warn(msg)
	}
	l.execPlugin(zapcore.WarnLevel, msg)
}

func (l *MulLogger) ErrorMsg(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	if l.moduleName != "" {
		l.logger.Error(msg, zap.Any("Module", l.moduleName))
	} else {
		l.logger.Error(msg)
	}
	l.execPlugin(zapcore.ErrorLevel, msg)
}

func (l *MulLogger) PanicMsg(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	if l.moduleName != "" {
		l.logger.Panic(msg, zap.Any("Module", l.moduleName))
	} else {
		l.logger.Panic(msg)
	}
	l.execPlugin(zapcore.PanicLevel, msg)
}

func (l *MulLogger) DPanicMsg(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	if l.moduleName != "" {
		l.logger.DPanic(msg, zap.Any("Module", l.moduleName))
	} else {
		l.logger.DPanic(msg)
	}
	l.execPlugin(zapcore.PanicLevel, msg)
}

func (l *MulLogger) InfoMsg(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	if l.moduleName != "" {
		l.logger.Info(msg, zap.Any("Module", l.moduleName))
	} else {
		l.logger.Info(msg)
	}
	l.execPlugin(zapcore.InfoLevel, msg)
}

func (l *MulLogger) Debug(msg string, FieldValue interface{}) {
	//FieldValue可以是DxRecord,map[]interface等
	switch v := FieldValue.(type) {
	case *dxsvalue.DxValue:
		l.logger.Debug(msg, l.record2ZapField(v)...)
	case dxsvalue.DxValue:
		l.logger.Debug(msg, l.record2ZapField(&v)...)
	case map[string]interface{}:
		l.logger.Debug(msg, l.map2Field(v)...)
	case *map[string]interface{}:
		l.logger.Debug(msg, l.map2Field(*v)...)
	default:
		if l.moduleName != "" {
			l.logger.Debug(msg, zap.Any("Module", l.moduleName))
		} else {
			l.logger.Debug(msg)
		}
	}
	l.execPlugin(zapcore.DebugLevel, msg)
}

func (l *MulLogger) Warn(msg string, FieldValue interface{}) {
	//FieldValue可以是DxRecord,map[]interface等
	switch v := FieldValue.(type) {
	case *dxsvalue.DxValue:
		l.logger.Warn(msg, l.record2ZapField(v)...)
	case dxsvalue.DxValue:
		l.logger.Warn(msg, l.record2ZapField(&v)...)
	case map[string]interface{}:
		l.logger.Warn(msg, l.map2Field(v)...)
	case *map[string]interface{}:
		l.logger.Warn(msg, l.map2Field(*v)...)
	default:
		if l.moduleName != "" {
			l.logger.Warn(msg, zap.Any("Module", l.moduleName))
		} else {
			l.logger.Warn(msg)
		}
	}
	l.execPlugin(zapcore.WarnLevel, msg)
}

func (l *MulLogger) Error(msg string, FieldValue interface{}) {
	//FieldValue可以是DxRecord,map[]interface等
	switch v := FieldValue.(type) {
	case *dxsvalue.DxValue:
		l.logger.Error(msg, l.record2ZapField(v)...)
	case dxsvalue.DxValue:
		l.logger.Error(msg, l.record2ZapField(&v)...)
	case map[string]interface{}:
		l.logger.Error(msg, l.map2Field(v)...)
	case *map[string]interface{}:
		l.logger.Error(msg, l.map2Field(*v)...)
	default:
		if l.moduleName != "" {
			l.logger.Error(msg, zap.Any("Module", l.moduleName))
		} else {
			l.logger.Error(msg)
		}
	}
	l.execPlugin(zapcore.ErrorLevel, msg)
}

func (l *MulLogger) Panic(msg string, FieldValue interface{}) {
	//FieldValue可以是DxRecord,map[]interface等
	switch v := FieldValue.(type) {
	case *dxsvalue.DxValue:
		l.logger.Panic(msg, l.record2ZapField(v)...)
	case dxsvalue.DxValue:
		l.logger.Panic(msg, l.record2ZapField(&v)...)
	case map[string]interface{}:
		l.logger.Panic(msg, l.map2Field(v)...)
	case *map[string]interface{}:
		l.logger.Panic(msg, l.map2Field(*v)...)
	default:
		if l.moduleName != "" {
			l.logger.Panic(msg, zap.Any("Module", l.moduleName))
		} else {
			l.logger.Panic(msg)
		}
	}
	l.execPlugin(zapcore.PanicLevel, msg)
}

//开发模式下，Development=true时候，会记录日志，然后panic
func (l *MulLogger) DPanic(msg string, FieldValue interface{}) {
	//FieldValue可以是DxRecord,map[]interface等
	switch v := FieldValue.(type) {
	case *dxsvalue.DxValue:
		l.logger.DPanic(msg, l.record2ZapField(v)...)
	case dxsvalue.DxValue:
		l.logger.DPanic(msg, l.record2ZapField(&v)...)
	case map[string]interface{}:
		l.logger.DPanic(msg, l.map2Field(v)...)
	case *map[string]interface{}:
		l.logger.DPanic(msg, l.map2Field(*v)...)
	default:
		if l.moduleName != "" {
			l.logger.DPanic(msg, zap.Any("Module", l.moduleName))
		} else {
			l.logger.DPanic(msg)
		}
	}
	l.execPlugin(zapcore.DPanicLevel, msg)
}

func (l *MulLogger) Info(msg string, FieldValue interface{}) {
	switch v := FieldValue.(type) {
	case *dxsvalue.DxValue:
		l.logger.Info(msg, l.record2ZapField(v)...)
	case dxsvalue.DxValue:
		l.logger.Info(msg, l.record2ZapField(&v)...)
	case map[string]interface{}:
		l.logger.Info(msg, l.map2Field(v)...)
	case *map[string]interface{}:
		l.logger.Info(msg, l.map2Field(*v)...)
	default:
		if l.moduleName != "" {
			l.logger.Info(msg, zap.Any("Module", l.moduleName))
		} else {
			l.logger.Info(msg)
		}
	}
	l.execPlugin(zapcore.InfoLevel, msg)
}

func timeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
}

func (l *MulLogger) Printf(format string, args ...interface{}) {
	l.InfoMsg(format, args...)
}

func New(logcfg LogConfig, moduleName string, ctx context.Context) *MulLogger {
	var gatherItems [3]LogGatherItem
	idx := 0
	if logcfg.EnableConsole {
		gatherItems[idx].GatherStyle = LGS_Console
		gatherItems[idx].Level = logcfg.LogLevel
		idx++
	}
	if logcfg.LogPath != "" {
		//日志地址
		gatherItems[idx].GatherStyle = LGS_File
		gatherItems[idx].Level = logcfg.LogLevel
		gatherItems[idx].TargetPos = logcfg.LogPath
		idx++
	}

	if logcfg.LogsrvAddr != "" {
		//日志服务监听地址
		gatherItems[idx].GatherStyle = LGS_TCPServer
		gatherItems[idx].Level = logcfg.LogLevel
		gatherItems[idx].TargetPos = logcfg.LogsrvAddr
		idx++
	}
	if idx > 0 {
		return NewMultiLogger(logcfg.DebugMode, logcfg.ShowCaller, moduleName, ctx, gatherItems[:idx]...)
	}
	return nil
}

func NewMultiLogger(development bool, ShowCaller bool, moduleName string, ctx context.Context, gatherItems ...LogGatherItem) *MulLogger {
	encodinfo := zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     timeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	core := make([]zapcore.Core, len(gatherItems))
	idx := 0
	//var logsrv *SimpleLoggerSrv
	for _, item := range gatherItems {
		switch item.GatherStyle {
		case LGS_Console:
			consoleWriter := zapcore.AddSync(&ConsoleWriter{})
			core[idx] = zapcore.NewCore(zapcore.NewConsoleEncoder(encodinfo), consoleWriter, zap.LevelEnablerFunc(item.canLevelGather))
			idx++
		case LGS_File:
			if item.TargetPos == "" {
				continue
			}
			fileNameinfos := strings.FieldsFunc(item.TargetPos, func(r rune) bool {
				return r == '.'
			})
			fl := len(fileNameinfos)
			if fl > 1 { //有扩展名
				fileNameinfos[fl-2] = fileNameinfos[fl-2] + "%Y%m%d%H"
				fileNameinfos[fl-1] = "." + fileNameinfos[fl-1]
			}
			fileName := strings.Join(fileNameinfos, "")
			if item.TargetPos[0] == '.' {
				fileName = "." + fileName
			}
			filewriter, err := rotatelogs.New(fileName, rotatelogs.WithRotationTime(time.Hour*24), rotatelogs.WithMaxAge(time.Hour*24*7))
			if err != nil {
				writeSyncer, lowClose, err := zap.Open(item.TargetPos)
				if err == nil {
					core[idx] = zapcore.NewCore(zapcore.NewJSONEncoder(encodinfo), writeSyncer, zap.LevelEnablerFunc(item.canLevelGather))
					idx++
				} else if lowClose != nil {
					lowClose()
				}
			} else {
				core[idx] = zapcore.NewCore(zapcore.NewJSONEncoder(encodinfo), zapcore.AddSync(filewriter), zap.LevelEnablerFunc(item.canLevelGather))
				idx++
			}
		case LGS_MSGQueue:
			//发送到消息队列，比如说NSQ,KFK等,暂时不做
		case LGS_TCPServer:
			//自己建立自己的日志服务
			/*logsrv = &SimpleLoggerSrv{}
			err := logsrv.Open(item.TargetPos,ctx)
			if err == nil{
				fmt.Println("开启日志监听服务:",item.TargetPos)
				serverWriter := zapcore.AddSync(logsrv)
				core[idx] = zapcore.NewCore(zapcore.NewJSONEncoder(encodinfo),serverWriter,zap.LevelEnablerFunc(item.canLevelGather))
				idx++
			}*/
		}
	}
	if idx > 0 {
		var result MulLogger
		if development {
			if ShowCaller {
				result.logger = zap.New(zapcore.NewTee(core[:idx]...), zap.AddCaller(), zap.AddCallerSkip(1), zap.Development())
			} else {
				result.logger = zap.New(zapcore.NewTee(core[:idx]...), zap.Development())
			}
		} else if ShowCaller {
			result.logger = zap.New(zapcore.NewTee(core[:idx]...), zap.AddCaller(), zap.AddCallerSkip(1))
		} else {
			result.logger = zap.New(zapcore.NewTee(core[:idx]...))
		}
		result.showCaller = ShowCaller
		result.loggerPluginHandles = make([]LoggerPluginHandler, 0, 4)
		result.moduleName = moduleName
		return &result
	}
	return nil
}
